# wrla2024.gitlab.io

The 15th International Workshop on Rewriting Logic and its Applications (WRLA), Luxembourg, April 6-7, 2024.
Check https://wrla2024.gitlab.io/.